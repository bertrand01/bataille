<?php

namespace App;

use App\Decks\Card;

class Player
{
    private $name;
    /** @var Card[] */
    private $cards;
    private $points;

    public function __construct()
    {
        $this->points = 0;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function setName(string $name): Player
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @param Card[] $cards
     */
    public function setCards(array $cards): Player
    {
        $this->cards = $cards;

        return $this;
    }

    public function drawCard(): Card
    {
        return array_shift($this->cards);
    }

    public function addPoint(): Player
    {
        $this->points++;

        return $this;
    }

    public function getPoints(): int
    {
        return $this->points;
    }
}

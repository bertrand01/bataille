<?php

namespace App\Utils;

use App\Board;
use App\CardGames\Bataille;
use App\CardGames\CardGameFactory;

class Console
{
    private $availableCardGames = [
        Bataille::NAME,
    ];

    private $dialog;

    public function __construct(DialogInterface $dialog)
    {
        $this->dialog = $dialog;
    }

    public function execute(array $args): bool
    {
        if (count($args) <= 1) {
            $this->outputHelp();

            return false;
        }

        // Get the first command argument
        $game = $args[1];

        if (!in_array($game, $this->availableCardGames)) {
            $this->outputHelp();

            return false;
        }

        return $this->executeCardGame($game);
    }

    private function executeCardGame(string $game): bool
    {
        $cardGameFactory = new CardGameFactory();

        try {
            $cardGame = $cardGameFactory->create($this->dialog, $game);
        } catch (\Exception $e) {
            $this->dialog->error($e->getMessage());
            $this->outputHelp();

            return false;
        }

        $board = new Board($this->dialog, $cardGame);

        return $board->startGame();
    }

    private function outputHelp()
    {
        $this->dialog->warning('Jeux disponibles : ');

        foreach ($this->availableCardGames as $cardGame) {
            $this->dialog->warning($cardGame);
        }
    }
}

<?php

namespace App\Utils;

class Dialog implements DialogInterface
{
    public function write(string $message)
    {
        echo $message . PHP_EOL;
    }

    public function title(string $title)
    {
        echo '>>> ' .$title . PHP_EOL;
    }

    /**
     * Print $question in console and returns the given answer
     */
    public function ask(string $question, bool $required = true): string
    {
        if ($required) {
            $answer = '';

            while ('' === $answer) {
                $answer = readline($question);
            }

            return $answer;
        }

        return readline($question);
    }

    public function error(string $message)
    {
        echo "\033[1;31m" . $message . "\033[0m" . PHP_EOL;
    }

    public function warning(string $message)
    {
        echo "\033[1;33m" . $message . "\033[0m" . PHP_EOL;
    }

    public function success(string $message)
    {
        echo "\033[1;32m" . $message . "\033[0m" . PHP_EOL;
    }

    public function newLine()
    {
        echo PHP_EOL;
    }
}

<?php

namespace App\Utils;

interface DialogInterface
{
    public function write(string $message);
    public function title(string $title);
    public function ask(string $question, bool $required = true): string;
    public function error(string $message);
    public function warning(string $message);
    public function success(string $message);
    public function newLine();
}

<?php

namespace App;

use App\CardGames\CardGameInterface;
use App\Utils\DialogInterface;

class Board
{
    private $dialog;
    private $cardGame;

    public function __construct(DialogInterface $dialog, CardGameInterface $cardGame)
    {
        $this->dialog = $dialog;
        $this->cardGame = $cardGame;
    }

    public function startGame(): bool
    {
        if (!$this->cardGame->isValid()) {
            $this->dialog->error('Le jeu n\'est pas valide.');

            return false;
        }

        $this->dialog->title($this->cardGame->getName());
        $this->dialog->newLine();

        $this->cardGame->prepareDeck();

        $this->cardGame->setPlayers();
        $this->cardGame->setPlayersCards();

        // Play until there's no more card / reached the amount of rounds
        for ($i = 0; $i < $this->cardGame->getRounds(); $i++) {
            $this->play();
        }

        $this->endGame();

        return true;
    }

    protected function play()
    {
        $this->dialog->ask('Appuyer sur Entrée pour jouer la manche.', false);

        $this->cardGame->playRound();
    }

    protected function endGame()
    {
        $winner = $this->cardGame->getWinner();

        // Find and display winner's name
        $this->dialog->newLine();
        $this->dialog->success('###########################');
        $this->dialog->success('# Gagnant : ' . $winner->getName());
        $this->dialog->success('###########################');
    }
}

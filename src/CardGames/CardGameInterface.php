<?php

namespace App\CardGames;

use App\Player;

interface CardGameInterface
{
    public function getName(): string;
    public function prepareDeck(): CardGameInterface;
    public function setPlayers(): CardGameInterface;
    public function setPlayersCards(): CardGameInterface;
    public function playRound(): CardGameInterface;
    public function getRounds(): int;
    public function getWinner(): Player;
    public function isValid(): bool;
}

<?php

namespace App\CardGames;

use App\Decks\DeckInterface;
use App\Player;
use App\Utils\DialogInterface;

class Bataille implements CardGameInterface
{
    public const NAME = 'bataille';

    private $player1;
    private $player2;
    private $deck;
    private $dialog;

    public function __construct(DialogInterface $dialog, DeckInterface $deck)
    {
        $this->player1 = new Player();
        $this->player2 = new Player();
        $this->deck = $deck;
        $this->dialog = $dialog;
    }

    public function getName(): string
    {
        return ucfirst(static::NAME);
    }

    public function prepareDeck(): CardGameInterface
    {
        $this->deck->shuffle();

        return $this;
    }

    public function setPlayers(): CardGameInterface
    {
        $this->player1->setName($this->dialog->ask('Nom du joueur 1 : '));
        $this->player2->setName($this->dialog->ask('Nom du joueur 2 : '));
        $this->dialog->newLine();

        return $this;
    }

    public function setPlayersCards(): CardGameInterface
    {
        $rounds = $this->getRounds();

        // Split deck to give players an even number of cards
        // Players each have as many cards as there are rounds in the game
        $this->player1->setCards(array_slice($this->deck->getCards(), 0, $rounds));
        $this->player2->setCards(array_slice($this->deck->getCards(), $rounds, $rounds));

        return $this;
    }

    public function playRound(): CardGameInterface
    {
        // Each player draws a card
        $card1 = $this->player1->drawCard();
        $card2 = $this->player2->drawCard();

        // Here all cards just unique integers
        // Comparing them gives us the round winner
        if ($card1->getValue() > $card2->getValue()) {
            $this->player1->addPoint();
            $this->dialog->success($this->player1->getName() . ' : ' . $card1->getName());
            $this->dialog->write($this->player2->getName() . ' : ' . $card2->getName());
            $this->dialog->newLine();

            return $this;
        }

        $this->player2->addPoint();
        $this->dialog->write($this->player1->getName() . ' : ' . $card1->getName());
        $this->dialog->success($this->player2->getName() . ' : ' . $card2->getName());
        $this->dialog->newLine();

        return $this;
    }

    public function getRounds(): int
    {
        return $this->deck->getNumberOfCards() / 2;
    }

    public function getWinner(): Player
    {
        if ($this->player1->getPoints() > $this->player2->getPoints()) {
            return $this->player1;
        }

        if ($this->player1->getPoints() < $this->player2->getPoints()) {
            return $this->player2;
        }

        // Well, "There can be only one", as they say - and it's all about luck
        $players = [$this->player1, $this->player2];
        $luckyWinner = array_rand($players, 1);

        return $players[$luckyWinner];
    }

    public function isValid(): bool
    {
        $deckCardsNumber = $this->deck->getNumberOfCards();

        if (0 === $deckCardsNumber || $deckCardsNumber  % 2 !== 0) {
            return false;
        }

        return true;
    }
}

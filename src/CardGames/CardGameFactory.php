<?php

namespace App\CardGames;

use App\Decks\BasicDeck;
use App\Utils\DialogInterface;

class CardGameFactory
{
    public function create(DialogInterface $dialog, string $cardGame): CardGameInterface
    {
        switch ($cardGame) {
            case Bataille::NAME:
                return new Bataille($dialog, new BasicDeck());
            default:
                throw new \Exception('Jeu inconnu.');
        }
    }
}

<?php

namespace App\Decks;

class BasicDeck implements DeckInterface
{
    public const NUMBER_OF_CARDS = 52;

    /** @var Card[] */
    private $cards;

    public function __construct()
    {
        for ($i = 1; $i <= static::NUMBER_OF_CARDS; $i++) {
            $this->cards[] = new Card($i, $i);
        }
    }

    public function shuffle(): DeckInterface
    {
        shuffle($this->cards);

        return $this;
    }

    public function getCards(): array
    {
        return $this->cards;
    }


    public function getNumberOfCards(): int
    {
        return static::NUMBER_OF_CARDS;
    }
}

<?php

namespace App\Decks;

interface DeckInterface
{
    public function shuffle(): DeckInterface;
    public function getCards(): array;
    public function getNumberOfCards(): int;
}
